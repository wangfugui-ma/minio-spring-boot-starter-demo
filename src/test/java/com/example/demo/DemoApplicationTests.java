package com.example.demo;

import com.masiyi.minio.template.MinIOTemplate;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DemoApplicationTests {

    @Autowired
    MinIOTemplate minIOTemplate;

    @Test
    void contextLoads() {
        System.out.println(minIOTemplate.listBuckets());

    }

}
