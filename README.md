在上一篇文章中我们介绍了如何使用SpringBoot集成minio：
[我是怎么做到开源系统中的文件上传等功能的？](https://blog.csdn.net/csdnerM/article/details/121370125)
但是这个方式还是需要自己去写配置类去配置很多的东西，就在前不久我也写了一篇：
[我开源了一款阿里云OSS的spring-boot-starter](https://juejin.cn/post/7357979741667098658)
再然后我gitee仓库里面的这个项目 的star有点多：
[SpringBoot+MinIO](https://gitee.com/wangfugui-ma/spring-boot-minio)

![在这里插入图片描述](趁着无聊，写了一个minio-spring-boot-starter给大家使用img/c83d5c0b0ba54c888e4e32313624267e.png)
最后我之前又发布了如何发布项目到maven中央仓库的教程：
[如何发布jar包到maven中央仓库（2024年3月最新版保姆级教程）](https://juejin.cn/post/7347207466818289703)
在之前我已经教大家使用linux安装了一个minio
[Linux安装MinIO（图文解说详细版）](https://blog.csdn.net/csdnerM/article/details/121336618)

因为该有的条件都有了，万事俱备只欠东风，所以我就顺便写了一个`minio-spring-boot-starter`，并且发布到了maven的中央仓库供大家使用。


[maven仓库主页](https://central.sonatype.com/namespace/io.gitee.wangfugui-ma)

> ![在这里插入图片描述](趁着无聊，写了一个minio-spring-boot-starter给大家使用img/d1f1888f1a814e18b8db210223a0e983.png)


请使用`1.0.0`版本

## 第一步，新建一个SpringBoot项目

[SpringBoot入门：如何新建SpringBoot项目（保姆级教程）](https://juejin.cn/post/7350168797637656576)

## 第二步，在pom文件里面引入jar包



```java
<dependency>
    <groupId>io.gitee.wangfugui-ma</groupId>
    <artifactId>minio-spring-boot-starter</artifactId>
    <version>1.0.0</version>
</dependency>
```

## 第三步，配置你的minio信息

在yml或者properties文件中配置如下信息

```yml
minio:
  url: http://120.76.201.118:9000  #对象存储服务的URL
  accessKey: root #Access key账户
  secretKey: 123456.com  #Secret key密码
```

### minio.url


对象存储服务的URL，就是你搭建minio时的url：[Linux安装MinIO（图文解说详细版）](https://blog.csdn.net/csdnerM/article/details/121336618)


### minio.accessKey

accessKey，就是我们登录minio控制台的账户，这里我们设置了账户是root



> ![在这里插入图片描述](趁着无聊，写了一个minio-spring-boot-starter给大家使用img/c690f628e02046cd9e9c7574a66ffeec.png)



### minio.secretKey

相当于你minio的密码，就是我们上面控制台的密码


## 第四步，使用MinIOTemplate

```
    @Autowired
    MinIOTemplate minIOTemplate;
```

在你的项目中直接使用`Autowired`注解注入`MinIOTemplate `即可使用

> ![在这里插入图片描述](趁着无聊，写了一个minio-spring-boot-starter给大家使用img/846375d8ecb74206931cb1cd44469b7e.png)


## 第五步，验证是否可以使用

我们编写一个测试方法查询所有的bucket试试

```java
    @Test
    void contextLoads() {
        System.out.println(minIOTemplate.listBuckets());

    }
```

> ![这里是引用](趁着无聊，写了一个minio-spring-boot-starter给大家使用img/3e204c1ac7f3475bbcfb3b0188aa5eec.png)


可以看到这正是我们web页面看到的几个bucket。

>  ![在这里插入图片描述](趁着无聊，写了一个minio-spring-boot-starter给大家使用img/49df6d37ec564c348a2db4f0cfa3b9e5.png)


所以，这次相比我们比上次的方式优雅多了，只需要引入`minio-spring-boot-starter`之后再配置你的minio的信息，这样就可以使用了。

## 其他方法

1. `createBucket(String bucket)`: 创建一个新的存储桶。`bucket`是要创建的存储桶的名称。这个方法没有返回值。

2. `uploadFile(InputStream stream, String bucket, String objectName)`: 上传一个文件到指定的存储桶。`stream`是文件的输入流，`bucket`是存储桶的名称，`objectName`是文件在存储桶中的名称。这个方法没有返回值。

3. `listBuckets()`: 列出所有的存储桶。这个方法返回一个`List<String>`，包含所有存储桶的名称。

4. `listFiles(String bucket)`: 列出指定存储桶中的所有文件和目录。`bucket`是存储桶的名称。这个方法返回一个`List<Fileinfo>`，每个`Fileinfo`对象包含一个文件或目录的信息。

5. `download(String bucket, String objectName)`: 下载指定存储桶中的文件。`bucket`是存储桶的名称，`objectName`是文件在存储桶中的名称。这个方法返回一个`InputStream`，可以用于读取下载的文件。

6. `deleteBucket(String bucket)`: 删除指定的存储桶。`bucket`是要删除的存储桶的名称。这个方法没有返回值。

7. `deleteObject(String bucket, String objectName)`: 删除指定存储桶中的文件。`bucket`是存储桶的名称，`objectName`是要删除的文件的名称。这个方法没有返回值。

8. `copyObject(String sourceBucket, String sourceObject, String targetBucket, String targetObject)`: 复制一个文件到另一个存储桶。`sourceBucket`和`sourceObject`是源文件的存储桶和名称，`targetBucket`和`targetObject`是目标存储桶和名称。这个方法没有返回值。

9. `getObjectInfo(String bucket, String objectName)`: 获取指定文件的信息。`bucket`是存储桶的名称，`objectName`是文件的名称。这个方法返回一个`String`，包含文件的信息。

10. `getPresignedObjectUrl(String bucketName, String objectName, Integer expires)`: 生成一个用于HTTP GET请求的预签名URL。`bucketName`和`objectName`是要下载的文件的存储桶和名称，`expires`是URL的有效期（单位：秒）。这个方法返回一个`String`，是预签名URL。

11. `listAllFile()`: 获取MinIO中所有的文件的信息。这个方法没有入参，返回一个`List<Fileinfo>`，包含所有文件的信息。

12. 注意：其中的getMinioClient返回一个`MinioClient`，这样如果里面的封装好的方法不满足你的需求，可以自己使用MinioClient去编写你的代码，MinioClient是`io.minio`包下面的，也就是minio自己的类。
## 后续版本考虑支持

1. 添加更多丰富的api

> ![在这里插入图片描述](趁着无聊，写了一个minio-spring-boot-starter给大家使用img/7e5d6303bac33a439cb406fc76313585.png#pic_center)

